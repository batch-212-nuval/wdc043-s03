package com.zuitt.batch212;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A1 {


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int counter = 1;
        int answer = 1;
        int num = 0;

        try {
            System.out.println("Input an integer whose factorial will be computed");
            num = in.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Input is not a number");
        }
        catch (Exception e) {
            System.out.println("Invalid Input");
        }
        finally {
            while (counter <= num) {
                answer = answer * counter;
                counter++;

            }
            System.out.println("The factorial of " + num + " is " + answer);
        }


    }
}
